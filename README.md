## Perkenalan

	Nama Saya Hendrix

## Permintaan Maaf

	Sebelumnya saya terimakasih telah diberi kesempatan untuk mengerjakan interview ini. Bersamaan dengan ini juga saya mohon maaf tidak bisa menyelesaikan tugas yang diberikan karena saya bekerja full time di kantor dan belum bisa menyelesaikan tugas ini dengan sempurna, jadi hasil yang didapat tidak maksimal,

	Dengan ini saya mengundurkan diri, karena hal diatas.

	Jika ada kesempatan lagi saya akan mencoba lagi dengan saya yang baru dan lebih baik lagi, terimakasih telah diberi kesempatan

## Todo List

-   Webserver menggunakan `artisan serve`. **(Selesai)**
-   Validasi menggunakan Form Request Validation. **(Selesai)**
-   Tidak ada algoritma alur proses di Controller. **(Belum selesai)**
-   Transaksi basis data menggunakan Eloquent (MySQL/MariaDB). **(Tidak dikerjakan)**

	```bash
	 karena menurut saya data akan banyak makanya lebih baik menggunakan query untuk list datanya
	```

-   Dokumentasi REST-API menggunakan Swagger (l5-swagger). **(Belum selesai)**
-   Testing menggunakan PHPUnit. **(Belum selesai)**
-   Sertakan README sebagai petunjuk penggunaan.  **(Selesai)**

## installation

-   clone from git
-   do **composer install**
-   do **php artisan key:generate**
-   do **php artisan migrate**
-   do **php artisan db:seed**
-   do **php artisan test**
