<?php

use App\Http\Controllers\SettingsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// settings
Route::prefix('settings')->group(function () {
    Route::patch('', 'App\Http\Controllers\SettingsController@update');
});

// employees
Route::prefix('employees')->group(function () {
    Route::post('', 'App\Http\Controllers\EmployeesController@store');
    Route::get('', 'App\Http\Controllers\EmployeesController@index');
});

// overtimes
Route::prefix('overtimes')->group(function () {
    Route::post('', 'App\Http\Controllers\OvertimeController@store');
    Route::get('', 'App\Http\Controllers\OvertimeController@index');
});

Route::get('overtime-pays/calculate', 'App\Http\Controllers\OvertimePaysController@index');
