<?php

namespace App\Http\Controllers;

use App\Http\Requests\Overtime\Add;
use App\Http\Requests\Overtime\Get;
use App\Models\Employees;
use App\Models\Overtimes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class OvertimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Get $request)
    {
        $overtimes = DB::table('overtimes')
            ->whereBetween('date', [$request->date_started, $request->date_ended])
            ->get();
        foreach ($overtimes as $key => $overtime) {
            $overtime->employee = DB::table('employees')->select('name')->find($overtime->employee_id);
        }

        return response([
            'status' => 'Success',
            'data' => $overtimes,
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Add $request)
    {
        DB::beginTransaction();
        try {

            $overtime = new Overtimes();
            $overtime->employee_id =  $request->employee_id;
            $overtime->date = Carbon::parse($request->date)->format('Y-m-d');
            $overtime->time_started = Carbon::parse($request->date)->format('Y-m-d') . ' ' . $request->time_started . ':00';
            $overtime->time_ended = Carbon::parse($request->date)->format('Y-m-d') . ' ' . $request->time_ended . ':00';
            $overtime->save();

            DB::commit();

            return response([
                'status' => 'Success',
                'data' => $overtime,
            ], Response::HTTP_CREATED);
        } catch (\Throwable $th) {

            DB::rollBack();
            return response([
                'status' => 'Error',
                'message' => $th->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
