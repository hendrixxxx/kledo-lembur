<?php

namespace Database\Seeders;

use App\Models\Settings as ModelsSettings;
use Illuminate\Database\Seeder;

class settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'key' => 'overtime_method',
                'value' => 1,
                'expression' => '(salary / 173) * overtime_duration_total',
            ],
        ];

        foreach ($data as $key => $value) {
            ModelsSettings::updateOrCreate($value);
        }
    }
}
