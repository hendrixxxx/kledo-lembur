<?php

namespace App\Http\Controllers;

use App\Http\Requests\OvertimePay\Get;
use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class OvertimePaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Get $request)
    {

        $employees = DB::table('employees')
            ->select([
                'employees.*'
            ])
            ->join('overtimes', 'overtimes.employee_id', 'employees.id')
            ->whereMonth('overtimes.date', Carbon::parse($request->month)->format('m'))
            ->whereYear('overtimes.date', Carbon::parse($request->month)->format('Y'))
            ->distinct()
            ->get();

        foreach ($employees as $key => $employee) {

            $overtime_duration_total = 0;
            $amount = 0;

            $employee->status =  DB::table('references')
                ->select('name')
                ->where('references.id', $employee->status_id)
                ->first();

            $employee->overtimes = DB::table('overtimes')
                ->select([
                    'overtimes.*',
                    DB::raw("TIMESTAMPDIFF(HOUR, time_started, time_ended) as overtime_duration"),
                ])
                ->where('employee_id', $employee->id)
                ->whereMonth('date', Carbon::parse($request->month)->format('m'))
                ->whereYear('date', Carbon::parse($request->month)->format('Y'))
                ->get();

            foreach ($employee->overtimes as $key => $overtime) {
                if ($employee->status->name == 'Percobaan' && $overtime->overtime_duration > 1) {

                    $overtime_duration_total = $overtime_duration_total + $overtime->overtime_duration;
                    $overtime_duration_total = $overtime_duration_total - 1;
                } elseif ($employee->status->name == 'Tetap') {

                    $overtime_duration_total = $overtime_duration_total + $overtime->overtime_duration;
                }
            }

            $employee->overtime_duration_total = $overtime_duration_total;

            $setting = Settings::select([
                'references.name as reference_name',
                'settings.*'
            ])
                ->join('references', 'references.id', 'settings.value')
                ->first();

            if (!defined('salary')) define('salary', $employee->salary);
            if (!defined('overtime_duration_total')) define('overtime_duration_total', $overtime_duration_total);

            eval('$amount =' . $setting->expression . ';');

            $employee->amount = $amount;
        }


        return response([
            'status' => 'Success',
            'data' => $employees,
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
