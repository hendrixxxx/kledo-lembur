<?php

namespace Tests\Feature;

use App\Models\Employees;
use App\Models\References;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EmployeesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_created_employee()
    {
        $faker = Factory::create();

        $status = References::where('code', 'employee_status')->inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post('/api/employees', [
                "name" => $faker->name,
                "status_id" => $status->id,
                "salary" => $faker->numberBetween($min = 2000000, $max = 10000000),
            ]);

        $response->assertCreated();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list_employee()
    {

        $order_by = ['name', 'salary'];
        $order_type = ['ASC', 'DESC'];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->get('/api/employees?page=2&per_page=2&order_by=' . $order_by[rand(0, 1)] . '&order_type=' . $order_type[rand(0, 1)]);

        $response->assertOk();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_created_employee_min_2_character()
    {
        $faker = Factory::create();

        $status = References::where('code', 'employee_status')->inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post('/api/employees', [
                "name" => 'A',
                "status_id" => $status->id,
                "salary" => $faker->numberBetween($min = 2000000, $max = 10000000),
            ]);

        $response->assertStatus(422);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_created_employee_not_unique_name()
    {
        $faker = Factory::create();

        $status = References::where('code', 'employee_status')->inRandomOrder()->first();
        $employe = Employees::inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post('/api/employees', [
                "name" => $employe->name,
                "status_id" => $status->id,
                "salary" => $faker->numberBetween($min = 2000000, $max = 10000000),
            ]);

        $response->assertStatus(422);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_created_employee_not_integer_status_id()
    {
        $faker = Factory::create();

        $status = References::where('code', 'employee_status')->inRandomOrder()->first();
        $employe = Employees::inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post('/api/employees', [
                "name" => $faker->name,
                "status_id" => 'A',
                "salary" => $faker->numberBetween($min = 2000000, $max = 10000000),
            ]);

        $response->assertStatus(422);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_created_employee_not_found_status_id()
    {
        $faker = Factory::create();

        $status = References::where('code', 'employee_status')->inRandomOrder()->first();
        $employe = Employees::inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post('/api/employees', [
                "name" => $faker->name,
                "status_id" => 100,
                "salary" => $faker->numberBetween($min = 2000000, $max = 10000000),
            ]);

        $response->assertStatus(422);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_created_employee_not_integer_salary()
    {
        $faker = Factory::create();

        $status = References::where('code', 'employee_status')->inRandomOrder()->first();
        $employe = Employees::inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post('/api/employees', [
                "name" => $faker->name,
                "status_id" => $status->id,
                "salary" => $faker->numberBetween($min = 0, $max = 1999999),
            ]);

        $response->assertStatus(422);
    }
}
