<?php

namespace App\Http\Requests\Employee;

use App\Models\Employees;
use App\Models\References;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Add extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'String',
                'min:2',
                Rule::unique(Employees::class, 'name'),
            ],
            'status_id'  => [
                'Integer',
                Rule::exists(References::class, 'id')->where(function ($query) {
                    return $query->where([
                        ['code', '=', 'employee_status'],
                    ]);
                }),
            ],
            'salary'  => [
                'Integer',
                'min:2000000',
                'max:10000000',
            ]
        ];
    }
}
