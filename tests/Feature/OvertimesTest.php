<?php

namespace Tests\Feature;

use App\Models\Employees;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OvertimesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_created_overtimes()
    {
        $faker = Factory::create();
        $employee = Employees::inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post('/api/overtimes', [
                "employee_id" => $employee->id,
                "date" => Carbon::now()->format('Y-m-d'),
                "time_started" => Carbon::now()->format('H:i'),
                "time_ended" => Carbon::now()->addHours(2)->format('H:i'),
            ]);

        $response->assertCreated();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list_overtimes()
    {
        $faker = Factory::create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->call('GET', '/api/overtimes', [
                "date_started" => "2020-12-01",
                "date_ended" => "2020-12-21",
            ]);

        $response->assertOk();
        $response->getContent();

    }
}
