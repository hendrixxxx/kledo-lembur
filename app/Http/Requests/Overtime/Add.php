<?php

namespace App\Http\Requests\Overtime;

use App\Models\Overtimes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Add extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $employee_id = $this->employee_id;
        return [
            'employee_id'  => [
                'Integer',
                'exists:App\Models\Employees,id',

            ],
            'date' => [
                'date',
                Rule::unique('overtimes')
                    ->where(function ($query) use ($employee_id) {
                        return $query->where([
                            ['employee_id', $employee_id]
                        ]);
                    }),
            ],
            'time_started'  => 'date_format:H:i|before:time_ended',
            'time_ended'  => 'date_format:H:i|after:time_started'
        ];
    }
}
