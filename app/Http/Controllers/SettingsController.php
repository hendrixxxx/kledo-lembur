<?php

namespace App\Http\Controllers;

use App\Http\Requests\Settings\Update;
use App\Models\References;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class SettingsController extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request)
    {
        DB::beginTransaction();
        try {
            $setting = Settings::first();

            $referances = References::where([
                ['id', $request->value],
                ['code', $request->key]
            ])
                ->first();

            $setting->key = $request->key;
            $setting->value = $request->value;
            $setting->expression = $referances->expression;
            $setting->save();

            DB::commit();

            return response([
                'status' => 'Success',
                'data' => $setting,
            ], Response::HTTP_OK);
        } catch (\Throwable $th) {

            DB::rollBack();
            return response([
                'status' => 'Error',
                'message' => $th->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
