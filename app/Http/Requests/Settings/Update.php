<?php

namespace App\Http\Requests\Settings;

use App\Models\References;
use App\Models\Settings;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'key' => [
                'required',
                Rule::in(['overtime_method']),
            ],
            'value'  => [
                'required',
                Rule::exists(References::class, 'id')->where(function ($query) {
                    return $query->where([
                        ['code', '=', 'overtime_method'],
                    ]);
                }),
            ]
        ];
    }
}
