<?php

namespace Database\Seeders;

use App\Models\References as ModelsReferences;
use Illuminate\Database\Seeder;

class references extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'overtime_method',
                'name' => 'Salary / 173',
                'expression' => '(salary / 173) * overtime_duration_total',
            ],
            [
                'code' => 'overtime_method',
                'name' => 'Fixed',
                'expression' => '10000 * overtime_duration_total',
            ],
            [
                'code' => 'employee_status',
                'name' => 'Tetap',
                'expression' => null,
            ],
            [
                'code' => 'employee_status',
                'name' => 'Percobaan',
                'expression' => null,
            ]
        ];

        foreach ($data as $key => $value) {
            ModelsReferences::updateOrCreate($value);
        }

    }
}
