<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OvertimesPayTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list_overtimes_pay_calculate_Test()
    {
        $faker = Factory::create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->call('GET', '/api/overtime-pays/calculate', [
                "month" => "2020-12",
            ]);

        $response->assertOk();
        $response->getContent();
    }
}
