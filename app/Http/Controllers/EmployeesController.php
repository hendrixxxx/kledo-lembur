<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employee\Add;
use App\Http\Requests\PaginationRequest;
use App\Models\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class EmployeesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(PaginationRequest $request)
    {
        $employees = DB::table('employees')
            ->select([
                'employees.*',
            ])
            ->orderBy($request->order_by, $request->order_type)
            ->simplePaginate($request->per_page ?? 10);

        foreach ($employees as $key => $employee) {
            $employee->status =  DB::table('references')
                ->select('name')
                ->where('references.id', $employee->status_id)
                ->first();
        }

        $employees->appends($request->all());

        return response([
            'status' => 'Success',
            'data' => $employees,
        ], Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Add $request)
    {
        DB::beginTransaction();
        try {
            $employees = new Employees;
            $employees->name =  $request->name;
            $employees->status_id = $request->status_id;
            $employees->salary = $request->salary;
            $employees->save();

            DB::commit();

            return response([
                'status' => 'Success',
                'data' => $employees,
            ], Response::HTTP_CREATED);
        } catch (\Throwable $th) {

            DB::rollBack();
            return response([
                'status' => 'Error',
                'message' => $th->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
