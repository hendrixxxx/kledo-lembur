<?php

namespace Tests\Feature;

use App\Models\References;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SettingsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_update_setting()
    {
        $value = References::where('code', 'overtime_method')->inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->patch('/api/settings', [
                "key" => 'overtime_method',
                "value" => $value->id,
            ]);

        $response->assertOk();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_miss_key()
    {
        $value = References::where('code', 'overtime_method')->inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->patch('/api/settings', [
                "key" => 'overtime_',
                "value" => $value->id,
            ]);

        $response->assertStatus(422);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_miss_criteria()
    {
        $value = References::where('code', '!=', 'overtime_method')->inRandomOrder()->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])
            ->patch('/api/settings', [
                "key" => 'overtime_method',
                "value" => $value->id,
            ]);

        $response->assertStatus(422);
    }
}
